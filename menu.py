import pygame
import sys

pygame.init()

def menu(estado):
    #Criação
    screen = pygame.display.set_mode((300,300),0,32)

    #Fontes
    estilo1 = pygame.font.Font(None, 64)
    estilo2 = pygame.font.Font(None, 40)
    titulo = estilo1.render("Asteroids",1,(255,255,255))
    NovoJogo = estilo2.render("New Game",1,(255,255,255))
    Ajuda = estilo.render("Help",1,(255,255,255))
    Sair = estilo2.render("Exit",1,(255,255,255))

    #Imagens
    arquivo = pygame.image.load("background.jpg")
    fundo = pygame.transform.scale(arquivo,(400,400))
    
    while estado:
        # x- Sair
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
              # New Game
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed() == (1,0,0):
                    x,y = pygame.mouse.get_pos()
                    if (x > 80 and x < 110) and (y > 120 and y < 160):
                        return "jogo"
                        estado = False
            #Help
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed() == (1,0,0):
                    x,y = pygame.mouse.get_pos()
                    if (x > 110 and x < 140) and (y > 160 and y < 200):
                        return "ajuda"
                        estado = False
               #Sair         
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed() == (1,0,0):
                    x,y = pygame.mouse.get_pos()
                    if (x > 120 and x < 140) and (y > 205 and y < 220):
                        return "sair"
                        estado = False
        
        screen.blit(fundo,(0,0))
        screen.blit(titulo,(70,50))
        screen.blit(NovoJogo,(80,120))
        screen.blit(Ajuda,(110,160))
        screen.blit(Sair,(120,205))
        pygame.display.flip()

def ajuda(estado):
    #Criação
    screen = pygame.display.set_mode((600,600),0,32)

    #Fontes
    estilo1 = pygame.font.Font(None, 20)
    voltar = estilo1.render("Voltar",1,(255,255,255))
    
    #Imagens
    arquivo = pygame.image.load("ajuda.jpg")
    fundo = pygame.transform.scale(arquivo,(600,600))
    
    while estado:
        # x - sair
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            #Sair
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed() == (1,0,0):
                    x,y = pygame.mouse.get_pos()
                    if (x > 220 and x < 280) and (y > 300 and y < 320):
                        pygame.quit()
                        sys.exit()
             #Menu
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed() == (1,0,0):
                    x,y = pygame.mouse.get_pos()
                    if (x > 560 and x < 600) and (y > 580 and y < 600):
                        return "menu"
                        estado = False
                        
        screen.blit(fundo,(0,0))
        screen.blit(voltar,(560,580))
        pygame.display.flip()

def jogo(estado):
    #Criação
    screen = pygame.display.set_mode((600,600),0,32)
    # Ir para a screen do jogo
    import game.py
    while estado:
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
                        
        screen.fill((0,0,0))
        pygame.display.flip()
        
Switch = "menu"

while Switch == "menu":
    Switch = menu(True)
    if Switch == "sair":
        pygame.quit()
        sys.exit()
    while Switch == "ajuda":
        Switch = ajuda(True)
    while Switch == "jogo":
        Switch = jogo(True)
